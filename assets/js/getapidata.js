async function fetchData(){
    let response = await fetch("https://api.nomics.com/v1/currencies/ticker?key=2ae62545bbe20a35fc20f2a7cc5a95a9&ids=HEX&interval=1d,30d&convert=USD&per-page=100&page=1");
    let data = await response.json();
    data = JSON.stringify(data);
    data = JSON.parse(data);

    document.querySelector('#count0').innerHTML = data[0].price.substring(0,6);
    document.querySelector('#count1').innerHTML = (data[0].market_cap / 1000000000).toFixed(1);
    return data;
}

document.querySelector('#count2').innerHTML = (61911563558 / 1000000000).toFixed(2);
document.querySelector('#count3').innerHTML = "203,078";
   
fetchData();